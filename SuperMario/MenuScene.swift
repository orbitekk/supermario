//
//  MenuScene.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 26.08.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import SpriteKit
import GameplayKit
import UIKit

final class MenuScene: SKScene {
    var title: String?
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 500, height: 90))
        label.textAlignment = .center
        label.textColor = UIColor(white: 0.5, alpha: 1.0)
        label.font = UIFont.systemFont(ofSize: 80)
        label.textColor = UIColor.white
        return label
    }()
    
    private lazy var startButton: UIButton = {
        let startGameButton = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        startGameButton.setTitle("New game", for: .normal)
        startGameButton.tintColor = UIColor.white
        startGameButton.titleLabel?.font = UIFont.systemFont(ofSize: 30)
        startGameButton.addTarget(self, action: #selector(startGame), for: .touchUpInside)
        return startGameButton
    }()
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        view.addSubview(startButton)
        startButton.center = CGPoint(x: view.center.x, y: 400)
        startButton.isHidden = false
        
        view.addSubview(titleLabel)
        titleLabel.center = CGPoint(x: view.center.x, y: 150)
        titleLabel.text = title
    }
    
    @objc private func startGame() {
        startButton.isHidden = true
        titleLabel.isHidden = true
        
        let menuScene = SKScene(fileNamed: "GameScene")
        let transition = SKTransition.fade(withDuration: 1.0)
        menuScene!.scaleMode = .fill
        self.view!.presentScene(menuScene!, transition: transition)
    }
}
