//
//  Owl.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 26.03.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import Foundation
import SpriteKit

final class Owl: SKSpriteNode {
    private let textures = [
        SKTexture(imageNamed: "Owl1"),
        SKTexture(imageNamed: "Owl2")
    ]
    
    var frozen: Bool = true
    
    private var movingAction: SKAction?
    
    var previousDirection: MovingDirection = .none
    var movingDirection: MovingDirection = .left {
        didSet {
            previousDirection = oldValue
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        movingAction = SKAction.repeatForever(SKAction.animate(with: textures, timePerFrame: 0.5))
        run(movingAction!)
    }
    
    func update() {
        if frozen {
            physicsBody!.velocity.dx = 0
            return
        }
        switch movingDirection {
        case .left:
            if physicsBody!.velocity.dx > -100 {
                physicsBody!.velocity.dx -= 30
            }
        case .right:
            if physicsBody!.velocity.dx < 100 {
                physicsBody!.velocity.dx += 30
            }
        case .none:
            if physicsBody!.velocity.dx > 0 {
                physicsBody!.velocity = CGVector(dx: max(0, physicsBody!.velocity.dx - 30), dy: physicsBody!.velocity.dy)
                return
            }
            physicsBody!.velocity = CGVector(dx: min(0, physicsBody!.velocity.dx + 30), dy: physicsBody!.velocity.dy)
        }
    }
    
    func kill() {
        Game.shared.add(points: 50)
        let killAnimation = SKAction.scaleY(to: 0, duration: 0.3)
        run(killAnimation) { [weak self] in
            self?.removeFromParent()
        }
    }
}
