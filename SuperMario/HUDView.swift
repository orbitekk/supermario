//
//  HUDView.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 26.08.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import UIKit

final class HUDView: UIView {
    @IBOutlet private var pointsLabel: UILabel?
    @IBOutlet private var timeLabel: UILabel?
    @IBOutlet private var coinsLabel: UILabel?
    
    var points: Int {
        get {
            return 0
        }
        set {
            pointsLabel?.text = "\(newValue)"
        }
    }
    
    var time: Int {
        get {
            return 0
        }
        set {
            timeLabel?.text = "\(newValue)"
        }
    }
    
    var coins: Int {
        get {
            return 0
        }
        set {
            coinsLabel?.text = "\(newValue)"
        }
    }
}
