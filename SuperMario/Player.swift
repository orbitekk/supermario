//
//  Player.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 27.02.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import Foundation
import SpriteKit

final class Player: SKSpriteNode {
    private let texturesLeft = [
        SKTexture(imageNamed: "MarioLeft2"),
        SKTexture(imageNamed: "MarioLeft1"),
        SKTexture(imageNamed: "MarioLeft2"),
        SKTexture(imageNamed: "MarioLeft3")
    ]
    private let texturesRight = [
        SKTexture(imageNamed: "MarioRight2"),
        SKTexture(imageNamed: "MarioRight1"),
        SKTexture(imageNamed: "MarioRight2"),
        SKTexture(imageNamed: "MarioRight3")
    ]
    
    private var movingAction: SKAction?
    
    var previousDirection: MovingDirection = .none
    var movingDirection: MovingDirection = .none {
        didSet {
            previousDirection = oldValue
            refreshAnimation(jumpDidChange: false)
        }
    }
    var isOnGround: Bool {
        let contactedBodies = !physicsBody!.allContactedBodies().isEmpty
        return contactedBodies && -0.15...0.15 ~= Double(physicsBody?.velocity.dy ?? 0)
    }
    private(set) var isJumping = false {
        didSet {
            refreshAnimation(jumpDidChange: true)
        }
    }
    var lastContact = Date(timeIntervalSince1970: 0)
    
    func update() {
        switch movingDirection {
        case .left:
            if physicsBody!.velocity.dx > -300 {
                physicsBody!.velocity.dx -= 70
            }
        case .right:
            if physicsBody!.velocity.dx < 300 {
                physicsBody!.velocity.dx += 70
            }
        case .none:
            if physicsBody!.velocity.dx > 0 {
                physicsBody!.velocity = CGVector(dx: max(0, physicsBody!.velocity.dx - 70), dy: physicsBody!.velocity.dy)
                return
            }
            physicsBody!.velocity = CGVector(dx: min(0, physicsBody!.velocity.dx + 70), dy: physicsBody!.velocity.dy)
        }
        if isOnGround {
            if isJumping != false {
                isJumping = false
            }
        }
    }
    
    func jump() {
        if isOnGround {
            isJumping = true
            physicsBody!.applyImpulse(CGVector(dx: 0, dy: 465))
        }
    }
    
    private func refreshAnimation(jumpDidChange: Bool) {
        switch movingDirection {
        case .left:
            if previousDirection != .left || (jumpDidChange && !isJumping) {
                previousDirection = movingDirection
                if isOnGround {
                    movingAction = SKAction.repeatForever(SKAction.animate(with: texturesLeft, timePerFrame: 0.2))
                    run(movingAction!)
                } else {
                    removeAllActions()
                    texture = SKTexture(imageNamed: "MarioJumpLeft")
                }
            }
            else {
                if isJumping {
                    removeAllActions()
                    texture = SKTexture(imageNamed: "MarioJumpLeft")
                }
            }
        case .right:
            if previousDirection != .right || (jumpDidChange && !isJumping) {
                previousDirection = movingDirection
                if isOnGround {
                    movingAction = SKAction.repeatForever(SKAction.animate(with: texturesRight, timePerFrame: 0.2))
                    run(movingAction!)
                } else {
                    removeAllActions()
                    texture = SKTexture(imageNamed: "MarioJumpRight")
                }
            } else {
                if isJumping {
                    removeAllActions()
                    texture = SKTexture(imageNamed: "MarioJumpRight")
                }
            }
        case .none:
            removeAllActions()
            if isJumping {
                texture = SKTexture(imageNamed: "MarioJumpRight")
                return
            }
            texture = SKTexture(imageNamed: "Mario")
        }
    }
}
