//
//  Game.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 03.09.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import Foundation

final class Game {
    static let shared = Game()
    
    private(set) var time: Int = 0
    private(set) var coins: Int = 0
    private(set) var points: Int = 0
    
    private var timer: Timer?
    
    func start() {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
            self.time += 1
        })
    }
    
    func stop() {
        timer?.invalidate()
        timer = nil
    }
    
    func reset() {
        timer?.invalidate()
        timer = nil
        time = 0
        coins = 0
        points = 0
    }
    
    func incrementCoins() {
        self.coins += 1
        self.points += self.coins * 100
    }
    
    func add(points: Int) {
        self.points += points
    }
}
