//
//  GameScene.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 27.02.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    private var player: Player!
    private var owls: [Owl] = []
    private var flowers: [Flower] = []
    
    private lazy var hud: HUDView = {
        return UINib(nibName: "HUDView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! HUDView
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        physicsWorld.contactDelegate = self
    }
    
    fileprivate func showGameOver() {
        if let menuScene = SKScene(fileNamed: "MenuScene") as? MenuScene {
            menuScene.title = "Game Over"
            let transition = SKTransition.fade(withDuration: 1.0)
            menuScene.scaleMode = .fill
            self.view!.presentScene(menuScene, transition: transition)
            hud.removeFromSuperview()
        }
    }
    
    fileprivate func showWinScene() {
        if let winScene = SKScene(fileNamed: "WinScene") as? WinScene {
            let transition = SKTransition.fade(withDuration: 1.0)
            winScene.scaleMode = .fill
            self.view!.presentScene(winScene, transition: transition)
            hud.removeFromSuperview()
        }
    }
    
    override func didMove(to view: SKView) {
        player = childNode(withName: "Mario") as? Player
        owls = children.flatMap { node in
            guard let owl = node as? Owl else {
                return nil
            }
            owl.frozen = true
            return owl
        }
        flowers = children.flatMap { node in
            return node as? Flower
        }
        hud.center = CGPoint(x: view.center.x, y: 25)
        Game.shared.reset()
        hud.time = Game.shared.time
        hud.points = Game.shared.points
        hud.coins = Game.shared.coins
        Game.shared.start()
        view.addSubview(hud)
    }
    
    func touchDown(atPoint pos : CGPoint) {
        if pos.x > UIScreen.main.bounds.size.width*3/4 {
            player.movingDirection = .right
            return
        }
        if pos.x < UIScreen.main.bounds.size.width/4 {
            player.movingDirection = .left
            return
        }
        player.jump()
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        if pos.x > UIScreen.main.bounds.size.width*3/4 || pos.x < UIScreen.main.bounds.size.width/4 {
            player.movingDirection = .none
            return
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { touchDown(atPoint: t.location(in: nil)) }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { touchMoved(toPoint: t.location(in: nil)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { touchUp(atPoint: t.location(in: nil)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { touchUp(atPoint: t.location(in: nil)) }
    }
    
    override func update(_ currentTime: TimeInterval) {
        if let camera = camera {
            camera.position.x = max(player.position.x, camera.position.x)
            
            owls.forEach { owl in
                if camera.contains(owl) {
                    owl.frozen = false
                }
            }
        }
        player.update()
        owls.forEach {
            $0.update()
        }
        flowers.forEach {
            $0.update()
        }
        if player.position.y < -180 {
            Game.shared.stop()
            showGameOver()
        }
        hud.time = Game.shared.time
        hud.points = Game.shared.points
        hud.coins = Game.shared.coins
    }
}

extension GameScene: SKPhysicsContactDelegate {
    func didBegin(_ contact: SKPhysicsContact) {
        if contact.bodyA.node?.name == "Win" || contact.bodyB.node?.name == "Win" {
            showWinScene()
            return
        }
        if let mario = contact.bodyA.node as? Player {
            if let node = contact.bodyB.node as? Block {
                if Date().timeIntervalSince1970 - mario.lastContact.timeIntervalSince1970 < 0.1 {
                    return
                }
                if mario.position.y + mario.frame.size.height/2 - 15 <= node.position.y - node.frame.size.height/2 {
                    mario.lastContact = Date()
                    node.hit()
                }
            }
            if let owl = contact.bodyB.node as? Owl {
                handleCollisionWithEnemy(enemy: owl, player: mario)
                return
            }
            if let flower = contact.bodyB.node as? Flower {
                handleCollisionWithEnemy(enemy: flower, player: mario)
                return
            }
            return
        }
        if let mario = contact.bodyB.node as? Player {
            if let node = contact.bodyA.node as? Block {
                if Date().timeIntervalSince1970 - mario.lastContact.timeIntervalSince1970 < 0.1 {
                    return
                }
                if mario.position.y + mario.frame.size.height/2 - 15 <= node.position.y - node.frame.size.height/2 {
                    mario.lastContact = Date()
                    node.hit()
                }
            }
            if let owl = contact.bodyA.node as? Owl {
                handleCollisionWithEnemy(enemy: owl, player: mario)
                return
            }
            if let flower = contact.bodyA.node as? Flower {
                handleCollisionWithEnemy(enemy: flower, player: mario)
                return
            }
            return
        }
        if let tube = contact.bodyA.node, tube.name == "Tube" {
            if let owl = contact.bodyB.node as? Owl {
                if owl.movingDirection == .right {
                    owl.movingDirection = .left
                } else {
                    owl.movingDirection = .right
                }
            }
        }
        if let tube = contact.bodyB.node, tube.name == "Tube" {
            if let owl = contact.bodyA.node as? Owl {
                if owl.movingDirection == .right {
                    owl.movingDirection = .left
                } else {
                    owl.movingDirection = .right
                }
            }
        }
    }
    
    private func handleCollisionWithEnemy(enemy: Owl, player: Player) {
        if player.position.y - player.frame.size.height/2 + 15 >= enemy.position.y + enemy.frame.size.height/2 {
            player.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 170))
            enemy.kill()
            return
        }
        showGameOver()
    }
    
    private func handleCollisionWithEnemy(enemy: Flower, player: Player) {
        showGameOver()
    }
}
