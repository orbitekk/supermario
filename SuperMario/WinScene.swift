//
//  WinScene.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 13.09.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import SpriteKit
import GameplayKit
import UIKit

final class WinScene: SKScene {
    var title: String?
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 800, height: 90))
        label.textAlignment = .center
        label.textColor = UIColor(white: 0.5, alpha: 1.0)
        label.font = UIFont.systemFont(ofSize: 80)
        label.textColor = UIColor.white
        return label
    }()
    
    private lazy var scoreLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 500, height: 50))
        label.textAlignment = .center
        label.textColor = UIColor(white: 0.5, alpha: 1.0)
        label.font = UIFont.systemFont(ofSize: 45)
        label.textColor = UIColor.white
        return label
    }()
    
    private lazy var timeLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 500, height: 50))
        label.textAlignment = .center
        label.textColor = UIColor(white: 0.5, alpha: 1.0)
        label.font = UIFont.systemFont(ofSize: 45)
        label.textColor = UIColor.white
        return label
    }()
    
    private lazy var startButton: UIButton = {
        let startGameButton = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        startGameButton.setTitle("Play again", for: .normal)
        startGameButton.tintColor = UIColor.white
        startGameButton.titleLabel?.font = UIFont.systemFont(ofSize: 30)
        startGameButton.addTarget(self, action: #selector(startGame), for: .touchUpInside)
        return startGameButton
    }()
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        
        view.addSubview(startButton)
        startButton.center = CGPoint(x: view.center.x, y: 500)
        startButton.isHidden = false
        
        view.addSubview(titleLabel)
        titleLabel.center = CGPoint(x: view.center.x, y: 170)
        titleLabel.text = "Congratulations!"
        
        view.addSubview(scoreLabel)
        scoreLabel.center = CGPoint(x: view.center.x, y: 250)
        scoreLabel.text = "Points: \(Game.shared.points)"
        
        view.addSubview(timeLabel)
        timeLabel.center = CGPoint(x: view.center.x, y: 350)
        timeLabel.text = "Time: \(Game.shared.time)"
    }
    
    @objc private func startGame() {
        startButton.isHidden = true
        titleLabel.isHidden = true
        
        let menuScene = SKScene(fileNamed: "GameScene")
        let transition = SKTransition.fade(withDuration: 1.0)
        menuScene!.scaleMode = .fill
        self.view!.presentScene(menuScene!, transition: transition)
    }
}
