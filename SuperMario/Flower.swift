//
//  Flower.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 03.09.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import Foundation
import SpriteKit

final class Flower: SKSpriteNode {
    private let textures = [
        SKTexture(imageNamed: "Flower1"),
        SKTexture(imageNamed: "Flower2")
    ]
    
    private var movingAction: SKAction?
    
    private var startPosition: CGPoint?
    
    var previousDirection: MovingDirection = .none
    var movingDirection: MovingDirection = .right {
        didSet {
            previousDirection = oldValue
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        movingAction = SKAction.repeatForever(SKAction.animate(with: textures, timePerFrame: 0.5))
        run(movingAction!)
        startPosition = position
    }
    
    func update() {
        guard let startPosition = startPosition else {
            return
        }
        if position.y > startPosition.y {
            movingDirection = .right
        } else if position.y <= startPosition.y - 300 {
            movingDirection = .left
        }
        switch movingDirection {
        case .left:
            physicsBody!.velocity.dy = 100
        case .right:
            physicsBody!.velocity.dy = -100
        case .none:
            return
        }
    }
}
