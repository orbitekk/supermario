//
//  QuestionMark.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 28.02.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import Foundation
import SpriteKit

final class QuestionMark: SKSpriteNode {
    private var used: Bool = false
    private var coins: Int = 1
    
    func hit() {
        guard used == false else {
            return
        }
        used = true
        texture = SKTexture(image: #imageLiteral(resourceName: "UsedQuestionMark"))
        let up = SKAction.move(by: CGVector(dx: 0, dy: 20), duration: 0.15)
        up.timingMode = .easeOut
        let down = SKAction.move(by: CGVector(dx: 0, dy: -20), duration: 0.15)
        down.timingMode = .easeIn
        let sequence = SKAction.sequence([up, down])
        run(sequence)
        throwCoin()
    }
    
    func throwCoin() {
        let coin = Coin(imageNamed: "Coin")
        coin.position = position
        coin.zPosition = zPosition - 1
        coin.setScale(3)
        self.scene?.addChild(coin)
        coin.animate()
        Game.shared.incrementCoins()
    }
}

extension QuestionMark: Block {}
