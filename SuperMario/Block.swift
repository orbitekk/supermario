//
//  Block.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 11.03.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import Foundation
import CoreGraphics

protocol Block {
    var position: CGPoint { get }
    var frame: CGRect { get }
    
    func hit()
}
