//
//  Coin.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 11.03.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import Foundation
import SpriteKit

final class Coin: SKSpriteNode {
    func animate() {
        let scale1 = SKAction.scaleX(to: 0, duration: 0.4)
        let scale2 = SKAction.scaleX(to: 4, duration: 0.4)
        let scaleSequence = SKAction.sequence([scale1, scale2])
        let up = SKAction.move(by: CGVector(dx: 0, dy: 110), duration: 0.4)
        up.timingMode = .easeIn
        let down = SKAction.move(by: CGVector(dx: 0, dy: -110), duration: 0.4)
        down.timingMode = .easeOut
        let sequence = SKAction.sequence([up, down])
        let group = SKAction.group([sequence, scaleSequence])
        run(group) {
            self.removeFromParent()
        }
    }
}
