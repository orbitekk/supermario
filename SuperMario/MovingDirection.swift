//
//  MovingDirection.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 14.08.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import Foundation

enum MovingDirection {
    case none, left, right
}
