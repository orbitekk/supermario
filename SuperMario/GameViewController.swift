//
//  GameViewController.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 27.02.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        showMenuScene()
    }
    
    func showMenuScene() {
        if let view = self.view as! SKView? {
            if let scene = SKScene(fileNamed: "MenuScene") as? MenuScene {
                scene.title = "Super Mario"
                scene.scaleMode = .aspectFill
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
        }
    }
    
    func showGameScene() {
        if let view = self.view as! SKView? {
            if let scene = SKScene(fileNamed: "GameScene") {
                scene.scaleMode = .aspectFill
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
