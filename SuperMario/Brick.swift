//
//  Brick.swift
//  SuperMario
//
//  Created by Norbert Sroczyński on 11.03.2017.
//  Copyright © 2017 Norbert Sroczyński. All rights reserved.
//

import Foundation
import SpriteKit

final class Brick: SKSpriteNode {
    private var coins: Int = 0
    
    func hit() {
        if coins > 0 {
            throwCoin()
        }
        coins -= 1
        if coins <= 0 {
            removeFromParent()
            return
        }
        let up = SKAction.move(by: CGVector(dx: 0, dy: 20), duration: 0.15)
        up.timingMode = .easeOut
        let down = SKAction.move(by: CGVector(dx: 0, dy: -20), duration: 0.15)
        down.timingMode = .easeIn
        let sequence = SKAction.sequence([up, down])
        run(sequence)
    }
    
    func throwCoin() {
        let coin = Coin(imageNamed: "Coin")
        coin.position = position
        coin.zPosition = zPosition - 1
        coin.setScale(3)
        self.scene?.addChild(coin)
        coin.animate()
        Game.shared.incrementCoins()
    }
}

extension Brick: Block {}
